@file:Suppress("SpellCheckingInspection")

object Vers {
    // This library/app
    const val MPP_LIB = "0.1"

    // Other Salect libraries/apps
    const val OBJJSON = "0.27-SNAPSHOT"
    const val KOTLINIDIOM = "0.2-SNAPSHOT"

    // Jetbrains
    const val KOTLIN = "1.3.31"

    // Android version updates: see https://dl.google.com/dl/android/maven2/index.html
    const val APPCOMPAT = "1.0.0"
    const val CONSTRAINT_LAYOUT = "1.1.2"
    const val ESPRESSO_CORE = "3.1.0"
    const val TEST_RUNNER = "1.1.0"

    // Gradle Plugins
    const val ANDROID_LIBRARY = "3.3.2"
    const val ANDROID_EXTENSIONS = KOTLIN
}

object Android {
    // Build Script Android Block
    const val APPLICATION_ID = "nz.salect.mpp_lib"
    const val BUILD_TOOLS_VERSION = "28.0.3"
    const val COMPILE_SDK_VERSION = 28
    const val MIN_SDK_VERSION = 19
    const val TARGET_SDK_VERSION = 28
    const val VERSION_CODE = 1
    const val VERSION_NAME = "1.0"
    const val TEST_INSTRUMENT_RUNNER = "androidx.test.runner.AndroidJUnitRunner"
}

object Deps {
    //// ObjJSON library
    const val OBJJSON_COMMON = "nz.salect.objjson:objjson:${Vers.OBJJSON}"
    const val OBJJSON_IOS = "nz.salect.objjson:objjson-ios:${Vers.OBJJSON}"
    const val OBJJSON_JS = "nz.salect.objjson:objjson-js:${Vers.OBJJSON}"
    const val OBJJSON_JVM = "nz.salect.objjson:objjson-jvm:${Vers.OBJJSON}"

    //// KotlinIdiom library
    const val KOTLINIDIOM_COMMON = "nz.salect.kotlinidiom:kotlinidiom:${Vers.KOTLINIDIOM}"
    const val KOTLINIDIOM_ANDROID = "nz.salect.kotlinidiom:kotlinidiom-android:${Vers.KOTLINIDIOM}"
    const val KOTLINIDIOM_IOS = "nz.salect.kotlinidiom:kotlinidiom-ios:${Vers.KOTLINIDIOM}"
    const val KOTLINIDIOM_JVM = "nz.salect.kotlinidiom:kotlinidiom-jvm:${Vers.KOTLINIDIOM}"

    //// Android
    // Repo: https://dl.google.com/dl/android/maven2/index.html

    //// Android AppCompat Library V7
    // Documentation: https://developer.android.com/topic/libraries/support-library
    // Maven: https://mvnrepository.com/artifact/com.android.support/appcompat-v7/28.0.0
    const val APPCOMPAT = "androidx.appcompat:appcompat:${Vers.APPCOMPAT}"

    //// Android Support
    // Documentation: https://developer.android.com/topic/libraries/support-library
    // Maven: https://mvnrepository.com/artifact/com.android.support/appcompat-v7/28.0.0
    const val CONSTRAINT_LAYOUT = "androidx.constraintlayout:constraintlayout:${Vers.CONSTRAINT_LAYOUT}"

    //// Android Testing
    // Documentation: https://developer.android.com/training/testing

    //// Android Testing Support Library
    // Documentation: https://developer.android.com/training/testing/espresso/index.html
    // Maven: https://mvnrepository.com/artifact/com.android.support.test.espresso/espresso-core/3.0.2
    const val ESPRESSO_CORE = "androidx.test.espresso:espresso-core:${Vers.ESPRESSO_CORE}"

    //// Android Testing Support Library
    // Documentation: https://developer.android.com/training/testing/junit-runner.html
    // Maven: https://mvnrepository.com/artifact/com.android.support.test/runner/1.0.2
    const val TEST_RUNNER = "androidx.test:runner:${Vers.TEST_RUNNER}"
}

object Repos {
    const val SNAPSHOTS_URL = "https://oss.sonatype.org/content/repositories/snapshots/"
}

object Pom {
    // Required for publishing to Maven
    const val GROUP_ID = "nz.salect.mpp_lib"
    const val ARTIFACT_ID = "mpp_lib"
    const val VERSION = Vers.MPP_LIB
}
