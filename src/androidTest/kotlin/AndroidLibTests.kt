package sample

import nz.salect.kotlinidiom.utils.splitDateTime
import nz.salect.objjson.instanceFromJson
import kotlin.test.assertEquals
import kotlin.test.Test


data class MyAndroidDataClass(var item1: Int, var item2: String)

class ObjJSONLibAndroidTests {
    @Test
    fun testInstanceFromJson() {
        val testJSONData = """{
            item1: 123,
            item2: "Violet Crumble Bar"
        }
        """.trimIndent().instanceFromJson(MyAndroidDataClass::class)

        assertEquals(123, testJSONData.item1, "testInstanceFromJson item1")
        assertEquals("Violet Crumble Bar", testJSONData.item2, "testInstanceFromJson item2")
    }
}

class KotlinidiomLibAndroidTests {
    @Test
    fun testSplitDateTime() {
        val dateTest = "abc def".splitDateTime[0]
        assertEquals("abc", dateTest,"testSplitDateTime item1")
    }
}
