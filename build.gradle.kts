@file:Suppress("SpellCheckingInspection")

plugins {
    kotlin("multiplatform").version(Vers.KOTLIN)

    id("com.android.application").version(Vers.ANDROID_LIBRARY)
    kotlin("android.extensions").version(Vers.ANDROID_EXTENSIONS)
}

repositories {
    google()
    jcenter()
    mavenCentral()
    maven (Repos.SNAPSHOTS_URL)
}

group = Pom.GROUP_ID
version = Pom.VERSION

android {
    compileSdkVersion(Android.COMPILE_SDK_VERSION)
    // Enable ability to publish ALL build variants i.e. release & debug etc.
    //setPublishNonDefault(true)
    buildToolsVersion(Android.BUILD_TOOLS_VERSION)
    
    defaultConfig {
        // Library projects cannot/do not set "applicationId"
        applicationId = Android.APPLICATION_ID
        minSdkVersion(Android.MIN_SDK_VERSION)
        targetSdkVersion(Android.TARGET_SDK_VERSION)
        versionCode = Android.VERSION_CODE
        versionName = Android.VERSION_NAME
        testInstrumentationRunner = Android.TEST_INSTRUMENT_RUNNER
        // Publish "release" or "debug" build variant
        //defaultPublishConfig("debug")
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    buildTypes {
        getByName("debug") {
            isDebuggable = true
            // Enables code shrinking for the release build type
            isMinifyEnabled = false
        }
        getByName("release") {
            isDebuggable = false
            // Enables code shrinking for the release build type
            isMinifyEnabled = false
            //proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }
    testOptions {
        unitTests.apply {
            // By default, local unit tests throw an exception any time the code you are testing tries to access
            // Android platform APIs (unless you mock Android dependencies yourself or with a testing
            // framework like Mockito). However, you can enable the following property so that the test
            // returns either null or zero when accessing platform APIs, rather than throwing an exception.
            isReturnDefaultValues = true
        }
    }

    // Set main & test sources to new locations away from the Android defaults
    // i.e src/main/java & src/test/java
    sourceSets["main"].java.srcDirs("src/androidMain/kotlin")
    sourceSets["main"].res.srcDirs("src/androidMain/resources")
    //sourceSets["debug"].res.srcDirs("src/androidDebug/resources")
    //sourceSets["test"].java.srcDirs("src/androidTest/kotlin")

    // Set location of the AndroidManifest
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")

    // This does not seem to work for the androidTest instrument tests -- src/androidTest/java is it!
    sourceSets["androidTest"].java.srcDirs("src/androidTest/java")
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(Deps.APPCOMPAT)
    implementation(Deps.CONSTRAINT_LAYOUT)
    androidTestImplementation(Deps.TEST_RUNNER)
}

kotlin {
    android()

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
                implementation(Deps.OBJJSON_COMMON)
                implementation(Deps.KOTLINIDIOM_COMMON)
            }
        }

        val androidMain by getting {
            dependencies {
                implementation(kotlin("stdlib"))
            }
        }
        val androidTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
                implementation(Deps.OBJJSON_JVM)
                implementation(Deps.KOTLINIDIOM_JVM)
            }
        }
    }
}
